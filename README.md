# Geotool

A tool to view gps-tracks (.gpx, .fit) and correlate images using those tracks.

## Install Dependencies
```bash
pip install -r requirements.txt
```

## Run Geotool
Tested on Kubuntu 24.04
```bash
python main.py
```

## Screenshots
![alt text](screenshots/geotool.jpg "Geotool")
