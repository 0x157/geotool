import QtCore
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQml
import Qt.labs.platform as Platform
import "chartTypes.js" as CT
import "settings"

MenuBar {
    id: root
    property bool trackSelected: false

    signal clearMapCache
    signal smoothCurve(degree: string, sampleFactor: string, altitudeOnly: bool)
    signal resetSmoothTrack(altitudeOnly: bool)
    signal setTrackType(type: int)

    function addMapType(text, type) {
        if (type != null) {
            var newMapTypeAction = getMapTypeAction.createObject(mapTypesMenu, {text: text, maptype: type.style})
            mapTypesMenu.addAction(newMapTypeAction)
        }
    }

    Component {
        id: getMapTypeAction

        Action {
            id: mapTypeAction
            property int maptype
            checkable: true
            checked: MapSettings.mapType == maptype
            ActionGroup.group: mapTypeAlignmentGroup
            onTriggered: {
                MapSettings.mapType = maptype
            }
        }
    }

    Menu {
        title: qsTr("&File")
        Action {
            text: qsTr("&Open...")
            onTriggered: openTrackFiles.open()
        }
        Action {
            text: qsTr("&Save Selected Track As...")
            enabled: trackSelected
            onTriggered: saveTrack.open()
        }
        Action {
            text: qsTr("&Open Images...")
            onTriggered: openImageFiles.open()
        }
        MenuSeparator { }
        Action {
            text: qsTr("&Settings")
            onTriggered: {
                settingsWindow.visible = true
            }
        }
        MenuSeparator { }
        Action {
            text: qsTr("&Quit")
            onTriggered: Qt.quit()
        }

        Platform.FileDialog {
            id: openTrackFiles
            title: "Open files"
            fileMode: Platform.FileDialog.OpenFiles
            folder: Platform.StandardPaths.standardLocations(Platform.StandardPaths.HomeLocation)[0]
            selectedNameFilter.index: 1
            nameFilters: ["Supported files (*.gpx *.fit)", "GPS Exchange Format files (*.gpx)", "Garmin Flexible and Interoperable Data Transfer files (*.fit)", "All files (*)"]
            onAccepted: {
                if (files.length > 0) {
                    app.addGeoFiles(files)
                }
            }
        }

        Platform.FileDialog {
            id: openImageFiles
            title: "Open image files"
            fileMode: Platform.FileDialog.OpenFiles
            folder: Platform.StandardPaths.standardLocations(Platform.StandardPaths.HomeLocation)[0]
            selectedNameFilter.index: 1
            nameFilters: ["Image files (*.jpg *.jpeg *.arw *.dng)"]
            onAccepted: {
                if (files.length > 0) {
                    app.addImages(files)
                }
            }
        }

        Platform.FileDialog {
            id: saveTrack
            title: "Save File"
            fileMode: Platform.FileDialog.SaveFile
            folder: Platform.StandardPaths.standardLocations(Platform.StandardPaths.HomeLocation)[0]
            selectedNameFilter.index: 1
            nameFilters: ["GPS Exchange Format files (*.gpx)"]
            onAccepted: app.saveSelectedTrack(files[0])
        }
    }
    Menu {
        id: mapTypesMenu
        title: qsTr("&Map Types")
        ActionGroup { id: mapTypeAlignmentGroup }
    }
    Menu {
        id: chartTypesMenu
        title: qsTr("&Track")
        Action {
            text: qsTr("Smooth Track")
            enabled: trackSelected
            onTriggered: {
                smoothingWindow.visible = true
            }
        }
        MenuTextSeparator { text: "Chart Types" }
        ActionGroup { id: chartAlignmentGroup }
        Action {
            text: qsTr("&Distance - Altitude")
            checkable: true
            checked: true
            ActionGroup.group: chartAlignmentGroup
            onTriggered: { setTrackType(CT.DISTANCE_ALTITUDE) }
        }
        Action {
            text: qsTr("&Distance - Speed")
            checkable: true
            checked: false
            ActionGroup.group: chartAlignmentGroup
            onTriggered: { setTrackType(CT.DISTANCE_SPEED) }
        }
        Action {
            text: qsTr("&Duration - Altitude")
            checkable: true
            checked: false
            ActionGroup.group: chartAlignmentGroup
            onTriggered: { setTrackType(CT.DURATION_ALTITUDE) }
        }
        Action {
            text: qsTr("&Duration - Speed")
            checkable: true
            checked: false
            ActionGroup.group: chartAlignmentGroup
            onTriggered: { setTrackType(CT.DURATION_SPEED) }
        }
    }
    Menu {
        title: qsTr("&Help")
        Action { text: qsTr("&About") }
    }

    SmoothingWindow {
        id: smoothingWindow

        Component.onCompleted: {
            smoothingWindow.smoothCurve.connect(root.smoothCurve)
            smoothingWindow.resetSmoothTrack.connect(root.resetSmoothTrack)
        }
    }

    SettingsWindow {
        id: settingsWindow
        Component.onCompleted: settingsWindow.clearMapCache.connect(root.clearMapCache)
    }
}
