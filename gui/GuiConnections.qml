import QtQuick

import "ImagePanel"

Connections {
    property TrackMap map
    property TrackList trackPanel
    property TrackChart chartPanel
    property GeoMenu appMenu
    property ImagePanel imagePanel

    function onUpdateMap(mapTypeName) {
        map.updateMap(mapTypeName)
    }

    function onSetFocus(lat, lon) {
        map.setFocus(lat, lon)
    }

    function onAddMarker(name, lat, lon, markercolor, level) {
        map.addMarker(name, lat, lon, markercolor, level)
    }

    function onAddImageMarker(name, lat, lon, markercolor, level, imageSrc) {
        map.addImageMarker(name, lat, lon, markercolor, level, imageSrc)
    }

    function onAddTrackToMap(trackId, coords, color) {
        map.addTrack(trackId, coords, color)
    }

    function onRemoveMapItems(name) {
        map.removeMapItems(name)
    }


    function onAddTrackToList(trackId, name, desc, color) {
        trackPanel.addTrack(trackId, name, desc, color)
    }

    function onUpdateTrackOfList(trackId, name, desc, color) {
        trackPanel.updateTrack(trackId, name, desc, color)
    }

    function onSetTracksLoadingIndicator(running) {
        trackPanel.setBusyIndicator(running)
    }


    function onSetTrackChart(trackId, coords, xmin, xmax, xunit, ymin, ymax, yunit, color) {
        chartPanel.visible = true

        if (chartPanel.implicitHeight == 0) {
            chartPanel.implicitHeight = chartPanel.lastImplictheight
            chartPanel.height = chartPanel.lastHeight
            chartPanel.SplitView.preferredHeight = chartPanel.lastHeight
        }
        appMenu.trackSelected = true
        chartPanel.setTrackChart(trackId, coords, xmin, xmax, xunit, ymin, ymax, yunit, color)
    }

    function onClearTrackChart() {
        appMenu.trackSelected = false
        chartPanel.lastImplictheight = chartPanel.implicitHeight
        chartPanel.lastHeight = chartPanel.height
        chartPanel.clearTrackChart()
        chartPanel.visible = false
    }


    function onAddImage(imgId, name, path, coords, datetaken) {
        imagePanel.visible = true
        imagePanel.addImage(imgId, name, path, coords, datetaken)
    }

    function onSetImage(imgId, name, path, coords, datetaken) {
        imagePanel.setImage(imgId, name, path, coords, datetaken)
    }

    function onUpdateImageProgress(from, to, value) {
        imagePanel.updateProgress(from, to, value)
    }
}
