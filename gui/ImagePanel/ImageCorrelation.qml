import QtQuick
import QtQuick.Controls


Rectangle {
    id: imageCorrelation

    function updateProgress(from, to, value) {
        correlationProgress.visible = to != value
        correlationProgress.from = from
        correlationProgress.to = to
        correlationProgress.value = value
    }

    Column {
        spacing: 10
        leftPadding: 4

        Row {
            spacing: 4

            Button {
                text: "Correlate"
                onClicked: {
                    var offset = timeOffset.text.length == 0 ? timeOffset.placeholderText : timeOffset.text
                    var gap = timeGap.text.length == 0 ? timeGap.placeholderText : timeGap.text

                    var replaceMode = app.CORRELATE_REPLACE_NONE()
                    if (keepCoords.currentText == "Exif") {
                        replaceMode = app.CORRELATE_KEEP_EXIF()
                    } else if (keepCoords.currentText == "None") {
                        replaceMode = app.CORRELATE_REPLACE_ALL()
                    }

                    app.correlateImages(offset, gap, replaceMode)
                }
            }

            Button {
                text: "Write Geo Information"
                onClicked: {
                    app.writeImageGeoInformation(keepOrignal.checked)
                }
            }
        }

        ProgressBar {
            id: correlationProgress
            visible: false
            width: imageCorrelation.width - 10
        }

        Grid {
            columns: 2
            spacing: 4
            verticalItemAlignment: Grid.AlignVCenter

            Text {
                id: utcOffset
                text: "UTC Time Offset:"

                ToolTip.visible: showOffsetDetails.hovered
                ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
                ToolTip.text: "(+|-) hh
(+|-) hh:mm
(+|-) hh:mm:ss"

                HoverHandler {
                    id: showOffsetDetails
                    acceptedPointerTypes: PointerDevice.AllPointerTypes
                }
            }

            TextField {
                id: timeOffset
                width: 80
                placeholderText: "00:00:00"
                placeholderTextColor: "grey"
                z: 5
            }


            Text {
                text: "Time Gap (seconds):"
            }

            TextField {
                id: timeGap
                width: 30
                placeholderText: "600"
                placeholderTextColor: "grey"
                validator: IntValidator{}
            }


            Text {
                text: "Keep Coordinates:"

                ToolTip.visible: showKeepCoordsDetails.hovered
                ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
                ToolTip.text: "Specifies which existing coordinates should not be recalculated"
                HoverHandler {
                    id: showKeepCoordsDetails
                    acceptedPointerTypes: PointerDevice.AllPointerTypes
                }
            }

            ComboBox {
                id: keepCoords
                width: 80
                model: ["All", "Exif", "None"]
            }
        }

        CheckBox {
            id: keepOrignal
            text: "Keep copy of original images"
            checked: true
        }
    }
}
