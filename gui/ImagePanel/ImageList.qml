import QtQuick
import QtQuick.Controls
import QtQuick.Shapes
import Qt.labs.qmlmodels

import ".."
import "../utils/utils.js" as Utils


Rectangle {
    id: root

    SystemPalette { id: colorPalette; colorGroup: SystemPalette.Active }

    property bool imageGrabbed: false
    property color topRowColor: colorPalette.window
    property color highlightColor : colorPalette.highlight

    signal imageMarkerDropped(imgId: int, globalX: real, globalY: real)

    function updateColumnsWidths(name, coords, datetaken) {
        pictureTable.maxWidthCol0 =
            Math.max(Utils.getWidthOfText(name), pictureTable.maxWidthCol0)
        pictureTable.maxWidthCol1 =
            Math.max(Utils.getWidthOfText(coords), pictureTable.maxWidthCol1)
        pictureTable.maxWidthCol2 =
            Math.max(Utils.getWidthOfText(datetaken), pictureTable.maxWidthCol2)

        // TODO maybe not that performant when called with each addImage/setImage
        pictureTable.forceLayout() // refresh row/column sizes.
    }

    function addImage(imgId, name, path, coords, datetaken) {
        pictureTableContent.appendRow({"objectName": imgId,
            "info": {"name": name, "path": path},
            "coords": coords,
            "date": datetaken})
        updateColumnsWidths(name, coords, datetaken)
    }

    function setImage(imgId, name, path, coords, datetaken) {
        var idx = Utils.getRowIndex(pictureTableContent, imgId)
        pictureTableContent.setRow(idx, {"objectName": imgId,
            "info": {"name": name, "path": path},
            "coords": coords,
            "date": datetaken})
        updateColumnsWidths(name, coords, datetaken)
    }

    function selectImage(imgId) {
        var row = Utils.getRowIndex(pictureTableContent, imgId)
        if (row != -1) {
            handleImageRowSelected(pictureTable.selectedRow, row, false)
        }
    }

    function handleImageRowSelected(prevRow, currRow, centerOnMap) {
        if (prevRow != -1 && prevRow != currRow) {
            var pr = pictureTableContent.getRow(prevRow)
            app.resetImageMarker(pr.objectName)
        }

        if (currRow != -1) {
            pictureTable.selectedRow = currRow;
            var r = pictureTableContent.getRow(currRow)
            app.focusOnImage(r.objectName, centerOnMap)
        }
    }

    HorizontalHeaderView {
        id: horizontalHeader
        anchors.left: pictureTable.left
        anchors.top: parent.top
        syncView: pictureTable
        clip: true

        columnSpacing: 1
        rowSpacing: 1
        model: [ "Filename","Coordinates","Date Taken"]

        delegate: Rectangle {
            color: topRowColor

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (pictureTable.selectedRow != -1) {
                        var pr = pictureTableContent.getRow(pictureTable.selectedRow)
                        app.resetImageMarker(pr.objectName)
                    }
                    pictureTable.selectedRow = -1;
                    imageListSelection.clear()
                }
            }

            Text {
                id: tableTitle
                text: modelData
                anchors.left: parent.left
                font.pointSize: 12
                font.bold: true
                leftPadding: 2
                rightPadding: 2
                topPadding: 1
                bottomPadding: 1
                z: 2
            }

            implicitWidth: tableTitle.width
            implicitHeight: tableTitle.height

            ToolTip.visible: tableTitle.text == "Coordinates" && showOffsetDetails.hovered
            ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
            ToolTip.text: "latitude,longitude[@altitude]"

            HoverHandler {
                id: showOffsetDetails
                acceptedPointerTypes: PointerDevice.AllPointerTypes
            }
        }
    }

    TableView {
        id: pictureTable
        anchors.top: horizontalHeader.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        width: root.width
        height: root.height
        clip: true
        interactive: true

        ScrollBar.horizontal: ScrollBar {}
        ScrollBar.vertical: ScrollBar {}

        property int selectedRow: -1
        property real maxWidthCol0: Utils.getWidthOfText("Filename", true)
        property real maxWidthCol1: Utils.getWidthOfText("Coordinates", true)
        property real maxWidthCol2: Utils.getWidthOfText("Date Taken", true)

        columnWidthProvider: function (column) {
            if (column == 0) {
                return maxWidthCol0 + 10
            } else if (column == 1) {
                return maxWidthCol1 + 10
            } else {
                return (maxWidthCol0 + maxWidthCol1 + maxWidthCol2 + 30) < root.width ?
                    root.width-(maxWidthCol0 + maxWidthCol1 + 20) : maxWidthCol2 + 10

            }
        }

        model: TableModel {
            id: pictureTableContent
            TableModelColumn { display: "info" }
            TableModelColumn { display: "coords" }
            TableModelColumn { display: "date" }
        }

        selectionModel: ItemSelectionModel {
            id: imageListSelection
            onCurrentChanged: (current, previous) => {
                handleImageRowSelected(previous.row, current.row, true)
            }
        }

        delegate: DelegateChooser{
            DelegateChoice {
                column: 0
                delegate: Rectangle {
                    id: filenameColumn
                    border.width: 0

                    color: row === pictureTable.selectedRow ?
                        highlightColor : (row%2 == 1 ? "azure" : "white")

                    Text {
                        id: filenameCell
                        text: display.name
                        anchors.left: parent.left
                        font.pointSize: 12
                        font.bold: false
                        leftPadding: 2
                        rightPadding: 2
                        topPadding: 1
                        bottomPadding: 1
                        z: 2
                    }
                    implicitHeight: filenameCell.height
                    implicitWidth: filenameCell.width + 10

                    ToolTip {
                        id: imageToolTip
                        visible: showImage.hovered && Utils.supportedImageFormat(display.path)
                            && !imageGrabbed
                        delay: Qt.styleHints.mousePressAndHoldInterval
                        contentItem: !Utils.supportedImageFormat(display.path) ?
                            Utils.createEmptyItem(imageToolTip) :
                            Utils.createImageToolTip(imageToolTip, display.path)
                    }

                    HoverHandler {
                        id: showImage
                        acceptedPointerTypes: PointerDevice.AllPointerTypes
                    }

                    MouseArea {
                        property Rectangle imgPtr: null
                        anchors.fill: parent

                        function getRoot() {
                            var p = parent
                            while (p.parent) {
                                p = p.parent
                            }
                            return p
                        }

                        onClicked: {
                            var r = getRoot()
                            handleImageRowSelected(pictureTable.selectedRow, row, true)
                        }

                        onPositionChanged: (mouse) => {
                            var g = mapToGlobal(mouse.x, mouse.y)
                            pictureTable.interactive = false
                            if (imgPtr) {
                                imgPtr.x = g.x-10
                                imgPtr.y = g.y-10
                            } else if (getRoot()) {
                                imageGrabbed = true
                                imgPtr = Utils.getImageDragIcon(getRoot(), g.x-10, g.y-10)
                            }
                        }

                        onReleased: (mouse) => {
                            if (imgPtr) {
                                var pr = pictureTableContent.getRow(row)
                                var imgId = pr.objectName
                                var pg = mapToGlobal(mouse.x, mouse.y)
                                root.imageMarkerDropped(imgId, pg.x, pg.y)
                                imgPtr.destroy()
                            }
                            imageGrabbed = false
                            imgPtr = null
                            pictureTable.interactive = true
                        }
                    }
                }
            }

            DelegateChoice {
                column: 1
                delegate: Rectangle {
                    id: coordinatesColumn
                    border.width: 0

                    color: row === pictureTable.selectedRow ?
                        highlightColor : (row%2 == 1 ? "azure" : "white")

                    TextInput {
                        id: coordinateCell
                        text: display
                        property string prevText: display
                        readOnly: false
                        wrapMode: Text.NoWrap
                        selectByMouse: true
                        anchors.left: parent.left
                        font.pointSize: 12
                        font.bold: false
                        leftPadding: 2
                        rightPadding: 2
                        topPadding: 1
                        bottomPadding: 1
                        z: 2
                        width: pictureTable.maxWidthCol1
                        validator: RegularExpressionValidator {
                            regularExpression: /\d{1,2}(\.\d+),\d{1,2}(\.\d+)(@\d+)?/
                        }
                        onEditingFinished: {
                            if (text != prevText) {
                                prevText = text
                                pictureTable.maxWidthCol1 =
                                    Math.max(Utils.getWidthOfText(text), pictureTable.maxWidthCol1)
                                pictureTable.forceLayout()
                                var pr = pictureTableContent.getRow(row)
                                app.setImageCoordinates(pr.objectName, text)
                            }
                            focus = false
                        }
                    }
                    implicitHeight: coordinateCell.height
                    implicitWidth: coordinateCell.width + 10
                }
            }

            DelegateChoice {
                column: 2
                delegate: Rectangle {
                    id: dateColumn
                    border.width: 0

                    color: row === pictureTable.selectedRow ?
                        highlightColor : (row%2 == 1 ? "azure" : "white")

                    Text{
                        id: dateCell
                        text: display
                        anchors.left: parent.left
                        font.pointSize: 12
                        font.bold: false
                        leftPadding: 2
                        rightPadding: 2
                        topPadding: 1
                        bottomPadding: 1
                        z: 2
                    }
                    implicitHeight: dateCell.height
                    implicitWidth: dateCell.width + 10
                }
            }
        }
    }

}
