import QtQuick
import QtQuick.Layouts

import ".."


Rectangle {
    id: root

    signal imageMarkerDropped(imgId: int, globalX: real, globalY: real)

    function addImage(imgId, name, path, coords, datetaken) {
        imageList.addImage(imgId, name, path, coords, datetaken)
    }

    function setImage(imgId, name, path, coords, datetaken) {
        imageList.setImage(imgId, name, path, coords, datetaken)
    }

    function selectImage(imgId) {
        imageList.selectImage(imgId)
    }

    function updateProgress(from, to, value) {
        imageCorrelation.updateProgress(from, to, value)
    }


    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        ImageList {
            id: imageList
            Layout.fillHeight: true
            Layout.fillWidth: true

            Component.onCompleted: imageList.imageMarkerDropped.connect(root.imageMarkerDropped)
        }

        ImageCorrelation {
            id: imageCorrelation
            height: 200
            implicitHeight: 200
            Layout.fillWidth: true
        }
    }
}
