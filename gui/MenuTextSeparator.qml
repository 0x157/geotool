import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

MenuSeparator {
    id: root

    SystemPalette { id: colorPalette; colorGroup: SystemPalette.Active }

    property string text: ""
    property string color: colorPalette.mid

    contentItem: TextSeparator {
        text: root.text
        color: root.color
        lineHeight: 2
    }
}
