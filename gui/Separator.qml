import QtQuick
import QtQuick.Layouts

RowLayout {
    id: root
    SystemPalette { id: colorPalette; colorGroup: SystemPalette.Active }
    property string color: colorPalette.mid
    property real lineHeight: 1

    Rectangle {
        color: root.color
        height: root.lineHeight
        Layout.fillWidth: true
    }
}
