import QtCore
import QtQuick
import QtQuick.Controls
import "settings"

Window {
    id: settingsWindow
    visible: false
    width: 300
    height: 185
    maximumWidth: width
    maximumHeight: height
    minimumWidth: width
    minimumHeight: height
    title: "Settings"
    flags: Qt.Dialog

    signal clearMapCache

    Column {
        id: rootcol
        property real mypadding: 4
        spacing: 10
        leftPadding: mypadding
        topPadding: mypadding

        TextSeparator {
            text: "Map Provider Information"
            lineHeight: 1
            width: settingsWindow.width - 2*rootcol.mypadding
        }

        CheckBox {
            id: localMapProvider
            text: "Local"
            checked: MapSettings.localMapProvider
        }

        Row {
            spacing: 10
            Text {
                text: "Remote Source: "
                enabled: !localMapProvider.checked
                anchors.verticalCenter: parent.verticalCenter
            }
            TextField {
                id: remoteMapProvider
                width: 180
                enabled: !localMapProvider.checked
                anchors.verticalCenter: parent.verticalCenter
                placeholderText: "http://geoproviders.0x157.eu"
                placeholderTextColor: "grey"
                text: MapSettings.remoteMapProvider
            }
        }

        Button {
            text: "Clear Map Cache"
            onClicked: clearMapCache()
        }

        Separator {
            lineHeight: 1
            width: settingsWindow.width - 2*rootcol.mypadding
        }

        Button {
            text: "Apply"
            anchors.right: parent.right
            onClicked: {
                MapSettings.localMapProvider = localMapProvider.checked
                MapSettings.remoteMapProvider = remoteMapProvider.text.length == 0 ?
                    remoteMapProvider.placeholderText : remoteMapProvider.text

                settingsWindow.close()
            }
        }
    }

}
