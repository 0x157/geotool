import QtQuick
import QtQuick.Controls

Window {
    id: smoothingWindow
    visible: false
    width: 200
    height: 150
    maximumWidth: width
    maximumHeight: height
    minimumWidth: width
    minimumHeight: height
    title: " "
    flags: Qt.Dialog

    signal smoothCurve(degree: string, sampleFactor: string, altitudeOnly: bool)
    signal resetSmoothTrack(altitudeOnly: bool)

    Column {
        spacing: 10
        leftPadding: 4

        Grid {
            columns: 2
            spacing: 4
            verticalItemAlignment: Grid.AlignVCenter

            Text {
                text: "Degree:"
            }
            TextField {
               id: degreeInput
               width: 30
               placeholderText: "3"
               placeholderTextColor: "grey"
           }

            Text {
                text: "Sample Factor:"
            }
            TextField {
               id: sfInput
               width: 30
               placeholderText: "3"
               placeholderTextColor: "grey"
           }
        }

        CheckBox {
            id: altitudeOnly
            text: "Apply to altitude"
            checked: true
        }

        Row {
            spacing: 4

            Button {
                text: "Smooth Curve"

                onClicked: {
                    var degree = degreeInput.text
                    if (degree.length==0) {
                        degree = degreeInput.placeholderText
                    }
                    var sampleFactor = sfInput.text
                    if (sampleFactor.length==0) {
                        sampleFactor = sfInput.placeholderText
                    }

                    smoothCurve(degree, sampleFactor, altitudeOnly.checked)
                }
            }

            Button {
                text: "Reset"
                onClicked: resetSmoothTrack(altitudeOnly.checked)
            }
        }
    }
}
