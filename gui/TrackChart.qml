import QtQuick
import QtQuick.Controls
import QtCharts

Rectangle {
    id: root

    property string currentTrackId: ""
    property AbstractSeries currentLineSeries: null

    function setTrackChart(trackId, coords, xmin, xmax, xunit, ymin, ymax, yunit, color) {
        myChart.visible = true
        if (coords.length>0)
        {
            // recompute line chart in case track has changed
            if (currentLineSeries != null) {
                myChart.removeSeries(currentLineSeries)
            }

            currentLineSeries = myChart.createSeries(
                ChartView.SeriesTypeLine, "Line series", axisX, axisY);

            // this loop takes a long time, unfortunately there's no way to append a list of points
            for (var i=0; i<coords.length; i++) {
                var x = coords[i].x
                var y = coords[i].y
                currentLineSeries.append(x, y)
            }

            currentLineSeries.color = color
        }

        root.currentTrackId = trackId

        axisX.min = xmin;
        axisX.max = xmax;
        axisX.labelFormat = `%.2f ${xunit}`

        axisY.min = ymin;
        axisY.max = ymax;
        axisY.labelFormat = `%.2f ${yunit}`
    }

    function clearTrackChart() {
        root.currentTrackId = ""
        myChart.removeAllSeries()
        myChart.visible = false
    }

    // TODO use GraphView in the future, I think it will provide better performance.
    // Available with Qt 6.7
    ChartView {
        id: myChart
        anchors.fill: parent
        antialiasing: true
        legend.visible: false
        margins.left: 0
        margins.right: 0
        margins.bottom: 0
        margins.top: 0

        ValueAxis {
            id: axisY
            gridVisible: true
        }

        ValueAxis {
            id: axisX
            gridVisible: true
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true

            property AbstractSeries vertLine
            property real lastMouseXPos: -1
            property real lastMouseYPos: -1
            property bool dragActive: false

            onClicked: (mouse)=> {
                if (!dragActive) {
                    var xm = mouse.x
                    var ym = mouse.y
                    var rx1 = myChart.plotArea.x
                    var rx2 = rx1 + myChart.plotArea.width
                    var ry1 = myChart.plotArea.y
                    var ry2 = ry1 + myChart.plotArea.height
                    if (xm >= rx1 && xm <= rx2 && ym >= ry1 && ym <= ry2) {
                        var width = rx2-rx1
                        var xpos = xm-rx1
                        var rel = xpos/width
                        app.handleFocusOnMapPoint(root.currentTrackId, rel)
                    }
                }
                dragActive = false
            }

            onDoubleClicked: {
                if (root.currentTrackId == "") {
                    return
                }

                app.handleTrackReset(root.currentTrackId)
            }

            onWheel: (wheelChange)=> {
                if (root.currentTrackId == "") {
                    return
                }

                // only zoom if mouse is within drawn area
                var xm = wheelChange.x
                var ym = wheelChange.y
                var rx1 = myChart.plotArea.x
                var rx2 = rx1 + myChart.plotArea.width
                var ry1 = myChart.plotArea.y
                var ry2 = ry1 + myChart.plotArea.height

                if (xm < rx1 || xm > rx2 || ym < ry1 || ym > ry2) {
                    return
                }

                var delta = wheelChange.angleDelta.y
                var absDelta = Math.abs(delta)
                absDelta = absDelta == 0 ? 0 : Math.log(absDelta)
                absDelta = absDelta/200
                var factor = 1
                if (delta < 0) {
                    factor = factor - absDelta
                } else {
                    factor = factor + absDelta
                }

                var width = rx2-rx1
                var xpos = xm-rx1
                var rel = xpos/width

                app.handleZoomTrack(root.currentTrackId, rel, factor)
            }


            function drawVerticalLine(mouse) {
                var xm = mouse.x
                var ym = mouse.y
                var rx1 = myChart.plotArea.x
                var rx2 = rx1 + myChart.plotArea.width
                var ry1 = myChart.plotArea.y
                var ry2 = ry1 + myChart.plotArea.height
                if (xm >= rx1 && xm <= rx2 && ym >= ry1 && ym <= ry2) {
                    var width = rx2-rx1
                    var xpos = xm-rx1
                    var rel = xpos/width

                    var xmin = axisX.min
                    var xmax = axisX.max
                    var x = xmin + (xmax-xmin) * rel

                    if (vertLine) {
                        myChart.removeSeries(vertLine)
                    }
                    vertLine = myChart.createSeries(
                        ChartView.SeriesTypeLine, "Line series", axisX, axisY)
                    vertLine.append(x, axisY.min)
                    vertLine.append(x, axisY.max)
                    vertLine.color = "grey"
                    vertLine.width = 2

                    lastMouseXPos = xm
                    lastMouseYPos = ym
                    app.handleAddMapMarkerFromChart(root.currentTrackId, rel)
                } else {
                    if (vertLine) {
                        myChart.removeSeries(vertLine)
                        vertLine = null
                        app.handleRemoveMapMarker(root.currentTrackId)
                    }
                    lastMouseXPos = -1
                    lastMouseYPos = -1
                }
            }

            function dragChart(mouse) {
                var xm = mouse.x
                var ym = mouse.y
                var rx1 = myChart.plotArea.x
                var rx2 = rx1 + myChart.plotArea.width
                var ry1 = myChart.plotArea.y
                var ry2 = ry1 + myChart.plotArea.height
                if (xm < rx1 || xm > rx2 || ym < ry1 || ym > ry2) {
                    return
                }

                var delta = xm - lastMouseXPos

                var width = rx2-rx1
                var oldxpos = xm-rx1
                var oldrel = oldxpos/width
                var xpos = (xm+delta)-rx1
                var rel = xpos/width

                lastMouseXPos = xm
                lastMouseYPos = ym
                app.handleDragTrack(root.currentTrackId, oldrel, rel)
            }

            onPositionChanged: (mouse)=> {
                if (root.currentTrackId == "") {
                    return
                }

                if (mouse.buttons == 1 && lastMouseXPos != -1) {
                    // mouse drag case
                    dragActive = true
                    dragChart(mouse)
                } else {
                    drawVerticalLine(mouse)
                }
            }
        }
    }
}
