import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Qt.labs.qmlmodels
import "chartTypes.js" as CT
import "utils/utils.js" as Utils

Rectangle {
    id: root

    SystemPalette { id: colorPalette; colorGroup: SystemPalette.Active }

    property int selectedTrackType: CT.DISTANCE_ALTITUDE
    property color topRowColor: colorPalette.window
    property color highlightColor : colorPalette.highlight


    function addTrack(trackId, name, desc, color) {
        trackTableContent.appendRow({
            "objectName": trackId,
            "color": color,
            "content": {"name": name, "details": desc}})
    }

    function updateTrack(trackId, name, desc, color) {
        var idx = Utils.getRowIndex(trackTableContent, trackId)
        if (idx != -1) {
            trackTableContent.setRow(idx, {
                "objectName": trackId,
                "color": color,
                "content": {"name": name, "details": desc}})
        }
    }

    function getSelectedTrackId() {
        if (trackTable.selectedRow == -1) {
            return null
        }

        var r = trackTableContent.getRow(trackTable.selectedRow)
        return r.objectName
    }

    function handleSelectedTrack() {
        var trackId = getSelectedTrackId()
        if (trackId != null) {
            map.focusMapOnItemCollection(trackId)
            app.handleTrackSelected(trackId, selectedTrackType)
        }
    }

    function setTrackType(type) {
        selectedTrackType = type
    }

    function setBusyIndicator(running) {
        tracksLoadingIndicator.running = running
    }


    Rectangle {
        anchors.fill: parent

        HorizontalHeaderView {
            id: horizontalHeader
            anchors.left: trackTable.left
            anchors.top: parent.top
            syncView: trackTable
            clip: true

            columnSpacing: 1
            rowSpacing: 1
            model: [ "","Track Name"]

            delegate: Rectangle {
                color: topRowColor

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (trackTable.selectedRow != -1) {
                            var r = trackTableContent.getRow(trackTable.selectedRow)
                            app.handleTrackUnselected(r.objectName)
                        }
                        trackTable.selectedRow = -1;
                        trackListSelection.clear()
                    }
                }

                Text {
                    id: tableTitle
                    text: modelData
                    anchors.left: parent.left
                    font.pointSize: 12
                    font.bold: true
                    leftPadding: 2
                    rightPadding: 2
                    topPadding: 1
                    bottomPadding: 1
                    z: 2
                }

                implicitWidth: tableTitle.width
                implicitHeight: tableTitle.height
            }
        }

        TableView {
            id: trackTable
            anchors.top: horizontalHeader.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            clip: true
            interactive: true

            width: root.width

            ScrollBar.horizontal: ScrollBar {}
            ScrollBar.vertical: ScrollBar {}

            property int selectedRow: -1
            property real maxWidthCol0: 20
            property real maxWidthCol1: 0

            columnWidthProvider: function (column) {
                if (column == 0) {
                    return maxWidthCol0
                }

                return maxWidthCol1 + 10 + 20 < root.width ?
                    root.width - 20 : maxWidthCol1 + 10
            }

            model: TableModel {
                id: trackTableContent
                TableModelColumn { display: "color" }
                TableModelColumn { display: "content" }
            }


            selectionModel: ItemSelectionModel {
                id: trackListSelection
                onCurrentChanged: (current, previous) => {
                    trackTable.selectedRow = current.row;
                    handleSelectedTrack()
                }
            }

            delegate: DelegateChooser{
                DelegateChoice {
                    column: 0
                    delegate: Rectangle {
                        id: trackColorColumn
                        border.width: 0

                        color: display

                        implicitWidth: 20
                        implicitHeight: 1
                    }
                }
                DelegateChoice {
                    id: trackColumn
                    column: 1
                    delegate: Rectangle {
                        id: trackColumnRect
                        border.width: 0
                        z: 1

                        color: (row === trackTable.selectedRow ?
                            highlightColor : (row%2 == 1 ? "azure" : "white"))

                        Text {
                            id: textCell
                            text: display.name
                            anchors.left: parent.left
                            font.pointSize: 12
                            font.bold: false
                            leftPadding: 2
                            rightPadding: 2
                            topPadding: 1
                            bottomPadding: 1
                            z: 2
                        }
                        implicitHeight: textCell.height
                        implicitWidth: textCell.width
                        Component.onCompleted: {
                            trackTable.maxWidthCol1 =
                                Math.max(textCell.width, trackTable.maxWidthCol1)
                        }

                        ToolTip.visible: showTrackDetails.hovered
                        ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
                        ToolTip.text: display.details
                        HoverHandler {
                            id: showTrackDetails
                            acceptedPointerTypes: PointerDevice.AllPointerTypes
                        }
                    }
                }
            }
        }

        BusyIndicator {
            id: tracksLoadingIndicator
            running: false
            anchors.centerIn: parent
        }
    }
}
