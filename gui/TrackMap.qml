import QtCore
import QtQuick
import QtQuick.Controls
import QtLocation
import QtPositioning
import QtQuick.Shapes 1.7
import "utils"
import "utils/utils.js" as Utils
import "settings"

Rectangle {

    id: root
    property string appPath

    signal imageClicked(id: string)

    Component.onDestruction: {
        var lat = map.center.latitude
        var lon = map.center.longitude
        MapSettings.centerLatitude = lat
        MapSettings.centerLongitude = lon
        MapSettings.zoomLevel = map.zoomLevel
    }

    Component {
        id: getPolyline
        MapPolyline{
            z: 1
            line.width: 3
        }
    }

    Component {
        id: getImageMarker
        MapQuickItem {
            id: markerItem
            property string markercolor
            property int level
            property string imageSrc
            z: level
            sourceItem: MarkerCircle {
                id: marker
                radius: 5.5
                color: markercolor

                ToolTip {
                    id: imageToolTip
                    visible: showImage.hovered && Utils.supportedImageFormat(imageSrc)
                    delay: Qt.styleHints.mousePressAndHoldInterval
                    contentItem: !Utils.supportedImageFormat(imageSrc) ? Utils.createEmptyItem(imageToolTip) : Utils.createImageToolTip(imageToolTip, imageSrc)
                }

                HoverHandler {
                    id: showImage
                    acceptedPointerTypes: PointerDevice.AllPointerTypes
                }

                MouseArea {
                    anchors.fill: parent
                    // todo improve the img ID handling ("img_")
                    onClicked: imageClicked(markerItem.objectName.replace("img_", ""))
                }
            }
            anchorPoint.x: marker.width/2
            anchorPoint.y: marker.height/2
        }
    }

    Component {
        id: getMarker
        MapQuickItem {
            id: markerItem
            property string markercolor
            property int level
            z: level
            sourceItem: MarkerCircle{
                id: marker
                radius: 5
                color: markercolor
            }
            anchorPoint.x: marker.width/2
            anchorPoint.y: marker.height/2
        }
    }

    function getRelevantMapTypes() {
        var mapTypes = []
        mapTypes.push(["Street Map", getMapType(MapType.StreetMap)])
        mapTypes.push(["Hiking Map", getMapType(MapType.PedestrianMap)])
        mapTypes.push(["Terrain Map", getMapType(MapType.TerrainMap)])
        mapTypes.push(["Satellite Map", getMapType(MapType.SatelliteMapDay)])
        mapTypes.push(["OSM Street Map", getMapType(MapType.TransitMap)])
        return mapTypes
    }

    function toCoordinate(pos) {
        return map.toCoordinate(pos, false)
    }

    function getMapItems(name) {
        var items = []
        for (var i=map.mapItems.length-1; i>=0; i--) {
            if (map.mapItems[i].objectName == name) {
                items.push(map.mapItems[i])
            }
        }
        return items
    }

    function focusMapOnItemCollection(name) {
        var items = getMapItems(name)
        map.fitViewportToMapItems(items)
    }

    function getMapType(type) {
        var supportedMapTypes = map.supportedMapTypes
        for (var i=0; i<supportedMapTypes.length; i++) {
            var t = supportedMapTypes[i]
            if (t.style==type && !t.night) {
                return t
            }
        }
        return null
    }

    function updateMap(type) {
        map.activeMapType = getMapType(type)
    }

    function setFocus(lat, lon) {
        var coord = QtPositioning.coordinate(lat, lon)
        map.center = coord
    }

    function addMarker(name, lat, lon, markercolor, level) {
        var coord = QtPositioning.coordinate(lat, lon)

        var marker = getMarker.createObject(map,
            {coordinate: coord, markercolor: markercolor, level: level})
        marker.objectName = name
        map.addMapItem(marker)
    }

    function addImageMarker(name, lat, lon, markercolor, level, imageSrc) {
        var coord = QtPositioning.coordinate(lat, lon)

        var marker = getImageMarker.createObject(map,
            {coordinate: coord, markercolor: markercolor, level: level, imageSrc: imageSrc})
        marker.objectName = name
        map.addMapItem(marker)
    }

    function addTrack(trackId, coords, color) {
        var pl = getPolyline.createObject(map)
        pl.objectName = trackId
        pl.line.color = color
        var path = [] //new list()
        for (var i=0; i<coords.length; i++) {
            path.push(coords[i])
            pl.addCoordinate(coords[i])
        }
        map.addMapItem(pl)
    }

    function removeMapItems(name) {
        for (var i=map.mapItems.length-1; i>=0; i--) {
            if (map.mapItems[i].objectName == name) {
                map.removeMapItem(map.mapItems[i])
            }
        }
    }

    function getMappingProviderAddress() {
        if (MapSettings.localMapProvider) {
            return "file:///" + appPath + "/osm_repository"
        } else {
            return MapSettings.remoteMapProvider
        }
    }

    function clearCache() {
        map.clearData()
    }

    function mapProviderChanged() {
        var address = getMappingProviderAddress()
        mapPluginParameter.value = address
    }

    function mapTypeChanged() {
        updateMap(MapSettings.mapType)
    }

    Component.onCompleted: {
        MapSettings.mapProviderChangedCallback.connect(mapProviderChanged)
        MapSettings.mapTypeChangedCallback.connect(mapTypeChanged)
    }

    Plugin {
        id: mapPlugin
        name: "osm"
        // repository structure definition: https://github.com/qt/qtlocation/blob/dev/src/plugins/geoservices/osm/qgeotileproviderosm.cpp#L365
        PluginParameter {
            id: mapPluginParameter
            name: "osm.mapping.providersrepository.address";
            value: getMappingProviderAddress()
        }
    }

    Map {
        id: map
        objectName: "mymap"
        anchors.fill: parent
        plugin: mapPlugin
        center: QtPositioning.coordinate(MapSettings.centerLatitude, MapSettings.centerLongitude)
        zoomLevel: MapSettings.zoomLevel
        property geoCoordinate startCentroid
        activeMapType: getMapType(MapSettings.mapType)

        PinchHandler { // https://doc.qt.io/qt-6/qml-qtquick-pinchhandler.html
            id: pinch
            target: null
            onActiveChanged: if (active) {
                map.startCentroid = map.toCoordinate(pinch.centroid.position, false)
            }
            onScaleChanged: (delta) => {
                map.zoomLevel += Math.log2(delta)
                map.alignCoordinateToPoint(map.startCentroid, pinch.centroid.position)
            }
            onRotationChanged: (delta) => {
                map.bearing -= delta
                map.alignCoordinateToPoint(map.startCentroid, pinch.centroid.position)
            }
            grabPermissions: PointerHandler.TakeOverForbidden
        }
        WheelHandler { // https://doc.qt.io/qt-6/qml-qtquick-wheelhandler.html
            id: wheel
            // workaround for QTBUG-87646 / QTBUG-112394 / QTBUG-112432:
            // Magic Mouse pretends to be a trackpad but doesn't work with PinchHandler
            // and we don't yet distinguish mice and trackpads on Wayland either
            acceptedDevices: Qt.platform.pluginName === "cocoa" || Qt.platform.pluginName === "wayland"
                             ? PointerDevice.Mouse | PointerDevice.TouchPad
                             : PointerDevice.Mouse
            onActiveChanged: if (active) {
                map.startCentroid = map.toCoordinate(wheel.point.position, false)
            }
            onWheel: (wheelChange) => { // https://doc.qt.io/qt-6/qml-qtquick-wheelevent.html
                var level = wheelChange.angleDelta.y/120/8
                map.zoomLevel += level
                // use the position of the pointer as the zoom position,
                // otherwise it wheel zoom to the center of the map
                map.alignCoordinateToPoint(map.startCentroid, wheel.point.position)
            }
        }
        DragHandler {
            id: drag
            target: null
            onTranslationChanged: (delta) => map.pan(-delta.x, -delta.y)
        }
        Shortcut {
            enabled: map.zoomLevel < map.maximumZoomLevel
            sequence: StandardKey.ZoomIn
            onActivated: map.zoomLevel = Math.round(map.zoomLevel + 1)
        }
        Shortcut {
            enabled: map.zoomLevel > map.minimumZoomLevel
            sequence: StandardKey.ZoomOut
            onActivated: map.zoomLevel = Math.round(map.zoomLevel - 1)
        }
    }
}
