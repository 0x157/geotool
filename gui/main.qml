import QtCore
import QtQuick
import QtQuick.Controls

import "ImagePanel"
// import directly from python application
import io.qt.geotool 1.0

// https://doc.qt.io/qt-5/qmlfirststeps.html#defining-custom-qml-types-for-re-use

ApplicationWindow {
    id: appWindow
    visible: true
    width: windowSettings.width
    height: windowSettings.height
    title: "Geo Tool"

    property QtObject guiAccess

    Settings {
        id: windowSettings
        category: "window"
        property real width: 1000
        property real height: 700
    }

    Component.onDestruction: {
        windowSettings.width = appWindow.width
        windowSettings.height = appWindow.height
    }

    ApplicationBridge {
        id: app
    }

    GuiConnections {
        target: guiAccess
        map: map
        trackPanel: trackPanel
        chartPanel: chartPanel
        appMenu: appMenu
        imagePanel: imagePanel
    }

    menuBar: GeoMenu {
        id: appMenu

        Component.onCompleted: {
            var mapTypes = map.getRelevantMapTypes()
            for (const entry of mapTypes) {
                appMenu.addMapType(entry[0], entry[1])
            }
        }

        onClearMapCache: map.clearCache()

        onSetTrackType: (type) => {
            trackPanel.setTrackType(type)
            trackPanel.handleSelectedTrack()
        }

        onSmoothCurve: (degree, sampleFactor, altitudeOnly) => {
            var trackId = trackPanel.getSelectedTrackId()
            if (trackId != null) {
                app.handleSmoothTrack(trackId, degree, sampleFactor, altitudeOnly, trackPanel.selectedChartType)
            }
        }

        onResetSmoothTrack: (altitudeOnly) => {
            var trackId = trackPanel.getSelectedTrackId()
            if (trackId != null) {
                app.handleResetSmoothTrack(trackId, altitudeOnly, trackPanel.selectedChartType)
            }
        }
    }

    SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal

        TrackList {
            id: trackPanel
            SplitView.preferredWidth: 200
        }

        SplitView {
            orientation: Qt.Vertical
            SplitView.fillWidth: true

            TrackMap {
                id: map
                appPath: applicationDirPath
                SplitView.fillHeight: true

                onImageClicked: (id) => imagePanel.selectImage(id)
            }

            TrackChart {
                id: chartPanel
                visible: false
                implicitHeight: 0
                property int lastHeight: 200
                property int lastImplictheight: 200
            }
        }

        ImagePanel {
            id: imagePanel
            visible: false
            SplitView.preferredWidth: 230

            onImageMarkerDropped: (imgId, globalX, globalY) => {
                var p = map.mapFromGlobal(globalX, globalY)
                if (p.x >= 0 && p.x <= map.width && p.y >= 0 && p.y <= map.height) {
                    var coord = map.toCoordinate(p)
                    var coordS = coord.latitude.toString().concat(
                        ",", coord.longitude.toString())
                    app.setImageCoordinates(imgId, coordS)
                }
            }
        }
    }

}
