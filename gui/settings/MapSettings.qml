pragma Singleton

import QtCore
import QtLocation

Settings {
    category: "map"
    property int mapType: MapType.StreetMap
    property bool localMapProvider: false
    property string remoteMapProvider: "http://geoproviders.0x157.eu"

    // Stuttgart
    property real centerLatitude: 48.7796
    property real centerLongitude: 9.1771

    property real zoomLevel: 14

    signal mapProviderChangedCallback
    signal mapTypeChangedCallback

    onLocalMapProviderChanged: mapProviderChangedCallback()
    onRemoteMapProviderChanged: mapProviderChangedCallback()
    onMapTypeChanged: mapTypeChangedCallback()
}
