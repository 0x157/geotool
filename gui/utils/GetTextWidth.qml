import QtQuick

TextMetrics {
    property bool bold
    font.pointSize: 12
    font.bold: bold
}
