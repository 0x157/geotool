import QtQuick
import QtQuick.Shapes

Rectangle {
    id: rect
    color: "transparent"
    width: 18
    height: 18
    Shape {
        ShapePath {
            strokeColor: "black"
            strokeWidth: 3
            startX: 0
            startY: 0
            PathLine { x: rect.width; y: rect.height }
        }
        ShapePath {
            strokeColor: "black"
            strokeWidth: 3
            startX: rect.width
            startY: 0
            PathLine { x: 0; y: rect.height }
        }
    }
}
