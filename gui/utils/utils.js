
function supportedImageFormat(path) {
    var ext = path.split(".").pop().toLowerCase()
    const supportedExtensions = ["jpg", "jpeg", "png", "bmp", "gif", "svg"]
    return supportedExtensions.includes(ext)
}

function createEmptyItem(parentItem) {
    var obj = `
        import QtQuick
        Item {}`
    return Qt.createQmlObject(obj, parentItem)
}

function createImageToolTip(parentItem, imgSource) {
    var obj = `
        import QtQuick

        Image {
            width: 256
            source: "${imgSource}"
            fillMode: Image.PreserveAspectFit
            sourceSize.width: 256
        }`
    return Qt.createQmlObject(obj, parentItem)
}

function getWidthOfText(text, bold=false) {
    var component = Qt.createComponent("GetTextWidth.qml");
    return component.createObject(null, {text: text, bold: bold}).width
}

function getImageDragIcon(parentObj, x, y) {
    var component = Qt.createComponent("ImageDragIcon.qml");
    return component.createObject(parentObj, {x: x, y: y})
}

function getRowIndex(contentTable, name) {
    for (var i=0; i<contentTable.rowCount; i++) {
        var r = contentTable.getRow(i)
        if (r.objectName == name) {
            return i
        }
    }
    return -1
}
