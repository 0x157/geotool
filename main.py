import sys
import os

from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtWidgets import QApplication

from src.geoApp import GeoApp
from src.appBridge import ApplicationBridge
from src.guiAccess import GuiAccess

app = QApplication(sys.argv)
app.setOrganizationName("0x157")
app.setOrganizationDomain("0x157")
app.setApplicationName("GeoTool")

engine = QQmlApplicationEngine()
engine.rootContext().setContextProperty("applicationDirPath", os.path.dirname(__file__))
engine.quit.connect(app.quit)
engine.load('gui/main.qml')

geoApp = GeoApp()
guiAccess = GuiAccess()
ApplicationBridge.setup(geoApp, guiAccess)

root = engine.rootObjects()[0]
root.setProperty('guiAccess', guiAccess)

sys.exit(app.exec())
