import os
from threading import Thread
from PySide6.QtQml import QmlElement
from PySide6.QtCore import QObject, Slot
from PySide6.QtPositioning import QGeoCoordinate

from . import correlate
from .utils import getCoordRep, getDateRep
from .chart import Chart

QML_IMPORT_NAME = "io.qt.geotool"
QML_IMPORT_MAJOR_VERSION = 1

def getImageReps(img):
    coords = getCoordRep(img.getCoords())
    datetaken = getDateRep(img.getDateTaken())

    return coords, datetaken

@QmlElement
class ApplicationBridge(QObject):
    COLORS = ['aqua', 'green', 'red', 'orange', 'yellow', 'blue', 'aquamarine', 'blueviolet', 'brown', 'burlywood', 'cadetblue', 'chartreuse', 'chocolate', 'coral', 'cornflowerblue', 'crimson', 'cyan', 'darkblue', 'darkcyan', 'darkgoldenrod', 'darkgray', 'darkgreen', 'darkkhaki', 'darkmagenta', 'darkolivegreen', 'darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen', 'darkslateblue', 'darkslategray', 'darkturquoise', 'darkviolet', 'deeppink', 'deepskyblue', 'dimgray', 'dodgerblue', 'firebrick', 'forestgreen', 'fuchsia', 'gold', 'goldenrod', 'gray', 'greenyellow', 'hotpink', 'indianred', 'indigo', 'lawngreen', 'lightblue', 'lightcoral', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon', 'lightseagreen', 'lightskyblue', 'lightslategray', 'lightsteelblue', 'lime', 'limegreen', 'magenta', 'maroon', 'mediumaquamarine', 'mediumblue', 'mediumorchid', 'mediumpurple', 'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise', 'mediumvioletred', 'olive', 'olivedrab', 'orangered', 'orchid', 'palegoldenrod', 'palegreen', 'paleturquoise', 'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink', 'plum', 'powderblue', 'purple', 'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen', 'sienna', 'silver', 'skyblue', 'slateblue', 'slategray', 'springgreen', 'steelblue', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'wheat', 'yellowgreen']
    guiAccess = None
    geoApp = None

    @Slot(result=int)
    def CORRELATE_REPLACE_NONE(self):
        return correlate.REPLACE_NONE

    @Slot(result=int)
    def CORRELATE_KEEP_EXIF(self):
        return correlate.KEEP_EXIF

    @Slot(result=int)
    def CORRELATE_REPLACE_ALL(self):
        return correlate.REPLACE_ALL

    def setup(geoApp, guiAccess):
        ApplicationBridge.geoApp = geoApp
        ApplicationBridge.guiAccess = guiAccess

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.addTracksToMapThread = None
        self.processImageMetadataThread = None

    def _waitForMapThread(self):
        if self.addTracksToMapThread and self.addTracksToMapThread.is_alive():
            self.addTracksToMapThread.join()

    def getColor(self, n):
        return self.COLORS[n%len(self.COLORS)]

    def addTracksToMap(self, trackIds):
        ApplicationBridge.guiAccess.triggerSetTracksLoadingIndicator(True)
        tracks = ApplicationBridge.geoApp.getTracks()
        lowestTrackId = None
        for trackId in trackIds:
            track = tracks[trackId]
            qGeoPoints = [QGeoCoordinate(p.latitude, p.longitude) for p in track.getPoints()]
            ApplicationBridge.guiAccess.triggerAddTrack(str(trackId), qGeoPoints, track.name, track.getDesc(), self.getColor(trackId))
            if not lowestTrackId or trackId < lowestTrackId:
                lowestTrackId = trackId

        focusPoint = tracks[lowestTrackId].getPoints()[0]
        ApplicationBridge.guiAccess.triggerSetFocus(focusPoint.latitude, focusPoint.longitude)
        ApplicationBridge.guiAccess.triggerSetTracksLoadingIndicator(False)

    @Slot(list)
    def addGeoFiles(self, files):
        self._waitForMapThread()
        ApplicationBridge.guiAccess.triggerSetTracksLoadingIndicator(True)
        trackIds = ApplicationBridge.geoApp.addTracks([f.path() for f in files])
        self.addTracksToMapThread = Thread(target=ApplicationBridge.addTracksToMap, args=(self, trackIds))
        self.addTracksToMapThread.start()

    @Slot(str, int)
    def handleTrackSelected(self, trackId, chartType):
        self._waitForMapThread()
        trackId = int(trackId)
        ApplicationBridge.geoApp.setSelectedTrack(trackId)
        chart = ApplicationBridge.geoApp.getChart(trackId, chartType)
        chart.resetView()
        chart.resetToOriginalData()
        ApplicationBridge.guiAccess.triggerSetTrackChart(trackId, chart, self.getColor(trackId))

    @Slot(str)
    def handleTrackUnselected(self, trackId):
        self._waitForMapThread()
        trackId = int(trackId)
        ApplicationBridge.geoApp.setSelectedTrack(None)
        ApplicationBridge.guiAccess.triggerClearTrackChart()

    @Slot(str)
    def handleTrackReset(self, trackId):
        trackId = int(trackId)
        chart = ApplicationBridge.geoApp.getChart(trackId)
        chart.resetView()
        ApplicationBridge.guiAccess.triggerSetTrackChart(trackId, chart, self.getColor(trackId))

    @Slot(str, float, float)
    def handleZoomTrack(self, trackId, relPos, zoomFactor):
        trackId = int(trackId)
        chart = ApplicationBridge.geoApp.getChart(trackId)
        chart.zoom(relPos, zoomFactor)
        ApplicationBridge.guiAccess.triggerSetTrackChart(trackId, chart, self.getColor(trackId))

    @Slot(str, float, float)
    def handleDragTrack(self, trackId, oldRelPos, relPos):
        trackId = int(trackId)
        chart = ApplicationBridge.geoApp.getChart(trackId)
        chart.drag(oldRelPos, relPos)
        ApplicationBridge.guiAccess.triggerSetTrackChart(trackId, chart, self.getColor(trackId))

    @Slot(str, float)
    def handleAddMapMarkerFromChart(self, trackId, relPos):
        trackId = int(trackId)
        chart = ApplicationBridge.geoApp.getChart(trackId)
        p = chart.getClosestGeoPoint(relPos)
        name = f"trackpoint-{trackId}"
        ApplicationBridge.guiAccess.triggerRemoveMapItems(name)
        ApplicationBridge.guiAccess.triggerAddMarker(name, p.latitude, p.longitude, self.getColor(trackId), 4)

    @Slot(str, float)
    def handleFocusOnMapPoint(self, trackId, relPos):
        trackId = int(trackId)
        chart = ApplicationBridge.geoApp.getChart(trackId)
        p = chart.getClosestGeoPoint(relPos)
        ApplicationBridge.guiAccess.triggerSetFocus(p.latitude, p.longitude)

    @Slot(str, str, str, bool, int)
    def handleSmoothTrack(self, trackId, degree, sampleFactor, altitudeOnly, chartType):
        trackId = int(trackId)
        degree = int(degree)
        sampleFactor = int(sampleFactor)

        track = ApplicationBridge.geoApp.getTrack(trackId)

        if altitudeOnly:
            track.smoothAltitude(degree, sampleFactor)
        else:
            track.smoothTrack(degree, sampleFactor)

        keepChartView = altitudeOnly or chartType in [Chart.DURATION_ALTITUDE, Chart.DURATION_SPEED]

        chart = ApplicationBridge.geoApp.getChart(trackId, cType=chartType, forceNew=True, keepChartView=keepChartView)
        qGeoPoints = [QGeoCoordinate(p.latitude, p.longitude) for p in track.getPoints()]
        ApplicationBridge.guiAccess.triggerUpdateTrackOnMap(trackId, qGeoPoints, track.name, track.getDesc(), self.getColor(trackId))
        ApplicationBridge.guiAccess.triggerSetTrackChart(trackId, chart, self.getColor(trackId))

    @Slot(str, bool, int)
    def handleResetSmoothTrack(self, trackId, altitudeOnly, chartType):
        trackId = int(trackId)

        track = ApplicationBridge.geoApp.getTrack(trackId)
        track.resetToOriginalData()

        keepChartView = altitudeOnly or chartType in [Chart.DURATION_ALTITUDE, Chart.DURATION_SPEED]

        chart = ApplicationBridge.geoApp.getChart(trackId, cType=chartType, forceNew=True, keepChartView=keepChartView)
        qGeoPoints = [QGeoCoordinate(p.latitude, p.longitude) for p in track.getPoints()]
        ApplicationBridge.guiAccess.triggerUpdateTrackOnMap(trackId, qGeoPoints, track.name, track.getDesc(), self.getColor(trackId))
        ApplicationBridge.guiAccess.triggerSetTrackChart(trackId, chart, self.getColor(trackId))

    @Slot(str)
    def handleRemoveMapMarker(self, trackId):
        trackId = int(trackId)
        name = f"trackpoint-{trackId}"
        ApplicationBridge.guiAccess.triggerRemoveMapItems(name)

    @Slot(str)
    def saveSelectedTrack(self, path):
        path = path.removeprefix("file://")
        self._waitForMapThread()
        track = ApplicationBridge.geoApp.getSelectedTrack()
        if track:
            track.saveAsGpx(path)

    def processImageMetadata(self):
        images = ApplicationBridge.geoApp.getImages().getImages().values()
        for img in images:
            changed = img.fillFromExif()
            if changed:
                coords, datetaken = getImageReps(img)
                ApplicationBridge.guiAccess.triggerSetImage(str(img.getId()), img.getName(), img.getImagePath(), coords, datetaken)
                self.addImageMarkerToMap(img)

    def processImageMetadataInThread(self):
        if self.processImageMetadataThread:
            self.processImageMetadataThread.join()

        self.processImageMetadataThread = Thread(target=ApplicationBridge.processImageMetadata, args=(self,))
        self.processImageMetadataThread.start()

    def addImageMarkerToMap(self, img, color="limegreen"):
        if img.hasCoords():
            markerId = f"img_{img.getId()}"
            coords = img.getCoords()
            ApplicationBridge.guiAccess.triggerRemoveMapItems(markerId)
            ApplicationBridge.guiAccess.triggerAddImageMarker(markerId, coords.latitude, coords.longitude, color, 2, img.getImagePath())

    def updateImageMarkerOnMap(self, img, color, level):
        markerId = f"img_{img.getId()}"
        ApplicationBridge.guiAccess.triggerRemoveMapItems(markerId)
        if img.hasCoords():
            coords = img.getCoords()
            ApplicationBridge.guiAccess.triggerAddImageMarker(markerId, coords.latitude, coords.longitude, color, level, img.getImagePath())

    @Slot(list)
    def addImages(self, files):
        imgIds = ApplicationBridge.geoApp.addImages([f.path() for f in files])
        images = ApplicationBridge.geoApp.getImages()
        for imgId in imgIds:
            img = images.getImage(imgId)
            coords, datetaken = getImageReps(img)
            ApplicationBridge.guiAccess.triggerAddImage(str(imgId), img.getName(), img.getImagePath(), coords, datetaken)
            self.addImageMarkerToMap(img)
        self.processImageMetadataInThread()

    @Slot(str, str, int)
    def correlateImages(self, timeOffset, timeGap, replaceMode):
        images = ApplicationBridge.geoApp.getImages().getImages().values()
        tracks = ApplicationBridge.geoApp.getTracks().values()

        images = correlate.correlateImages(images, tracks, offset=timeOffset, timeGapInSeconds=int(timeGap), replaceMode=replaceMode)
        for img in images:
            coords, datetaken = getImageReps(img)
            ApplicationBridge.guiAccess.triggerSetImage(str(img.getId()), img.getName(), img.getImagePath(), coords, datetaken)
            self.addImageMarkerToMap(img)

    @Slot(str, bool)
    def focusOnImage(self, imgId, centerOnMap):
        imgId = int(imgId)
        img = ApplicationBridge.geoApp.getImages().getImage(imgId)
        if img and img.hasCoords():
            c = img.getCoords()
            self.updateImageMarkerOnMap(img, "gold", 3)
            if centerOnMap:
                ApplicationBridge.guiAccess.triggerSetFocus(c.latitude, c.longitude)

    @Slot(str)
    def resetImageMarker(self, imgId):
        imgId = int(imgId)
        img = ApplicationBridge.geoApp.getImages().getImage(imgId)
        if img:
            self.updateImageMarkerOnMap(img, "limegreen", 2)

    @Slot(str, str)
    def setImageCoordinates(self, imgId, coords):
        imgId = int(imgId)
        img = ApplicationBridge.geoApp.getImages().getImage(imgId)
        if img:
            img.setCoordsFromString(coords)
            coords, datetaken = getImageReps(img)
            ApplicationBridge.guiAccess.triggerSetImage(str(img.getId()), img.getName(), img.getImagePath(), coords, datetaken)
            self.updateImageMarkerOnMap(img, "limegreen", 2)

    @Slot(bool)
    def writeImageGeoInformation(self, keepOriginal):
        def updateFct(f, t, v):
            ApplicationBridge.guiAccess.triggerUpdateImageProgress(f, t, v)
        img = ApplicationBridge.geoApp.getImages().writeGeoInformation(keepOriginal, updateFct)

