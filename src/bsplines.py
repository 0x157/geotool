from bisect import bisect_left

# based on introduction to B-Splines from: https://hovey.github.io/docs/Hovey_2022_Bezier_B-Spline_SAND2022-7702_C.pdf
# visual examples: https://pomax.github.io/BezierInfo-2/#bsplines

def bSplineSum1(agg, factor, point):
    return agg + factor * point

def bSplineSum2(agg, factor, point):
    return (agg[0] + factor * point[0],
            agg[1] + factor * point[1])

def bSplineSum3(agg, factor, point):
    return (agg[0] + factor * point[0],
            agg[1] + factor * point[1],
            agg[2] + factor * point[2])

def bSplineSumN(agg, factor, point):
    for i in range(len(point)):
        agg[i] = agg[i] + factor * point[i]
    return agg

class BSpline:
    def __init__(self, knotVec, controlPoints):
        self.knotVec = knotVec
        self.controlPoints = controlPoints

    def relevantKnots(self, p, t):
        i = bisect_left(self.knotVec, t) # v>=t for v in self.knotVec[i:]
        imin = max((i-1) - p - 1, 0)
        while self.knotVec[i] <= t:
            i += 1
        imax = i
        return imin, imax

    def base0(self, i, t):
        t1 = self.knotVec[i]
        t2 = self.knotVec[i+1]
        if t1 <= t and t < t2:
            return 1
        else:
            return 0

    def baseP(self, p, i, t):
        if p == 0:
            return self.base0(i, t)

        tmin = self.knotVec[i]
        tmax = self.knotVec[i+p+1]
        if t<tmin or t>=tmax:
            return 0

        bl = self.baseP(p-1, i, t)
        rl = 0
        if bl != 0:
            t1 = self.knotVec[i]
            t2 = self.knotVec[i+p]
            rl = ((t-t1)/(t2-t1)) * bl

        br = self.baseP(p-1, i+1, t)
        rr = 0
        if br != 0:
            t1 = self.knotVec[i+p+1]
            t2 = self.knotVec[i+1]
            rr = ((t1-t)/(t1-t2)) * br

        return rl + rr

    def curve(self, degree, t):
        if t == self.knotVec[-1]:
            return self.controlPoints[-1]
        imin, imax = self.relevantKnots(degree, t)
        #print("relevant range:", imin, imax)
        cp = self.controlPoints[0]
        bSplineSum = bSplineSum1
        result = 0
        if type(cp) is tuple:
            l = len(cp)
            result = (0,) * l
            if l == 2:
                bSplineSum = bSplineSum2
            elif l == 3:
                bSplineSum = bSplineSum3
            else:
                bSplineSum = bSplineSumN

        for i in range(imin, imax):# range(len(self.controlPoints)):
            b = self.baseP(degree, i, t)
            #if b != 0:
            #    print("base", i, b, self.controlPoints[i])
            result = bSplineSum(result, b, self.controlPoints[i])

        return result


def getKnotVec(p, values):
    start = [0.0] * (p+1)
    end   = [values[-1]] * (p+1)
    n = len(values)
    # getting knotpoints that correspond to controlpoints
    # kind of got loffset and roffset by trial and error
    loffset = (p+1)//2
    roffset = p-loffset
    result = values[loffset:n-p+roffset]
    return start + result + end


def getSamplePoints(values, step):
    result = []
    pos = 0
    while pos < len(values)-1:
        result.append(values[pos])
        pos += step
    result.append(values[-1])
    return result
