import bisect
import time
from .utils import getXDeltaDistance, getXDeltaDuration, getYValueAltitude, getYValueSpeed, getDistanceInfoFromMeters
from . import bsplines

GET_SMALLER = True
GET_LARGER = False

class ChartView:
    def __init__(self, xMin, xMax, yMin, yMax):
        self.xMin = xMin
        self.xMax = xMax
        self.yMin = yMin
        self.yMax = yMax

        self.values = []
        self.highestGranularity = False

    def update(self, xMin, xMax, yMin, yMax):
        self.xMin = xMin
        self.xMax = xMax
        self.yMin = yMin
        self.yMax = yMax

    @property
    def xMinStoredValues(self):
        return self.values[0][0]

    @property
    def xMaxStoredValues(self):
        return self.values[-1][0]

class Chart:

    DIST_ALTITUDE = 1
    DIST_SPEED = 3
    DURATION_ALTITUDE = 2
    DURATION_SPEED = 4

    DURATON_UNIT_SECONDS = "unitseconds"
    DISTANCE_UNIT_METERS = "meters"
    SPEED_UNIT_KMH = "kmh"

    def __init__(self, track, cType=DIST_ALTITUDE):
        self.track = track
        self.type = cType
        self.configure(cType)
        self.initChart()

    def initChart(self):
        self.chartValues = []
        self.origChartValues = None
        prevPoint = None
        totalLength = 0
        self.xMinValue = 0

        for point in self.track.getPoints():
            totalLength += self.getXDelta(point, prevPoint)
            yValue = self.getYValue(point, prevPoint)

            if len(self.chartValues) == 0:
                self.yMinValue = yValue
                self.yMaxValue = yValue
            else:
                self.yMinValue = min(self.yMinValue, yValue)
                self.yMaxValue = max(self.yMaxValue, yValue)

            self.chartValues.append((totalLength, yValue))
            prevPoint = point

        self.xMaxValue = totalLength

        self.resetView()

    def configure(self, variant):
        if variant == self.DIST_ALTITUDE:
            self.getXDelta = getXDeltaDistance
            self.getYValue = getYValueAltitude
            self.xUnit = self.DISTANCE_UNIT_METERS
            self.yUnit = self.DISTANCE_UNIT_METERS
        elif variant == self.DURATION_ALTITUDE:
            self.getXDelta = getXDeltaDuration
            self.getYValue = getYValueAltitude
            self.xUnit = self.DURATON_UNIT_SECONDS
            self.yUnit = self.DISTANCE_UNIT_METERS
        elif variant == self.DIST_SPEED:
            self.getXDelta = getXDeltaDistance
            self.getYValue = getYValueSpeed
            self.xUnit = self.DISTANCE_UNIT_METERS
            self.yUnit = self.SPEED_UNIT_KMH
        elif variant == self.DURATION_SPEED:
            self.getXDelta = getXDeltaDuration
            self.getYValue = getYValueSpeed
            self.xUnit = self.DURATON_UNIT_SECONDS
            self.yUnit = self.SPEED_UNIT_KMH
        else:
            self.getXDelta = getXDeltaDistance
            self.getYValue = getYValueAltitude
            self.xUnit = self.DISTANCE_UNIT_METERS
            self.yUnit = self.DISTANCE_UNIT_METERS

    def _getYMinMax(self, s, e):
        chart = self.chartValues[s:e]
        ymin = chart[0][1]
        ymax = ymin
        for _, y in chart:
            ymin = min(ymin, y)
            ymax = max(ymax, y)

        return ymin, ymax

    def getChartUnits(self):
        if self.xUnit == self.DISTANCE_UNIT_METERS:
            xUnit, xAdjustmentFactor, _ = getDistanceInfoFromMeters(self.xMinValue, self.xMaxValue)
        elif self.xUnit == self.DURATON_UNIT_SECONDS:
            xUnit, xAdjustmentFactor = ("sec", 1.0)

        if self.yUnit == self.DISTANCE_UNIT_METERS:
            yUnit, yAdjustmentFactor, _ = getDistanceInfoFromMeters(self.yMinValue, self.yMaxValue)
        elif self.yUnit == self.SPEED_UNIT_KMH:
            yUnit, yAdjustmentFactor = ("km/h", 1.0)

        return xUnit, xAdjustmentFactor, yUnit, yAdjustmentFactor

    def getClosestGeoPoint(self, relPos):
        chart = self.chartValues
        viewStartValue = self.chartView.xMin
        viewEndValue = self.chartView.xMax

        length = viewEndValue - viewStartValue
        relValue = viewStartValue + length*relPos

        pos = self.getClosestXPos(chart, relValue, GET_SMALLER)
        if pos < len(chart)-1:
            val1 = chart[pos][0]
            val2 = chart[pos+1][0]

            d1 = relValue-val1
            d2 = val2-relValue

            pos = pos if d1<d2 else pos+1

        return self.track.getPoints()[pos]

    def getClosestXPos(self, chart, xValue, useSmaller=GET_SMALLER):
        if len(chart) < 2:
            return 0

        i = bisect.bisect_left(chart, xValue, key=lambda e: e[0])

        xPosValue = chart[i][0]

        if useSmaller:
            if i == 0:
                return i

            return i if xValue == xPosValue else i-1
        else:
            if i >= len(chart)-1:
                return len(chart)-1

            return i if xValue == xPosValue else i+1

    def zoom(self, relPos, factor):
        chart = self.chartValues
        maxLength = chart[-1][0]

        oldStartValue = self.chartView.xMin
        oldEndValue = self.chartView.xMax

        oldLength = oldEndValue - oldStartValue
        relValue = oldStartValue + oldLength*relPos
        newLength = oldLength/factor

        # try to keep relPos at its current position
        leftLength = relPos*newLength
        rightLength = newLength - leftLength

        if relValue - leftLength < 0:
            startValue = 0
            endValue = min(newLength, maxLength)
        elif relValue + rightLength > maxLength:
            startValue = max(0, maxLength-newLength)
            endValue = maxLength
        else:
            startValue = relValue-leftLength
            endValue = relValue+rightLength


        spos = self.getClosestXPos(chart, startValue, GET_SMALLER)
        epos = self.getClosestXPos(chart, endValue, GET_LARGER)

        yMinValue, yMaxValue = self._getYMinMax(spos, epos+1)
        self.chartView.update(startValue, endValue, yMinValue, yMaxValue)

    def drag(self, oldRelPos, relPos):
        chart = self.chartValues
        maxLength = chart[-1][0]

        oldStartValue = self.chartView.xMin
        oldEndValue = self.chartView.xMax

        length = oldEndValue - oldStartValue

        relDelta = oldRelPos - relPos
        deltaValue = length * relDelta
        startValue = oldStartValue + deltaValue
        endValue = oldEndValue + deltaValue

        if startValue < 0:
            startValue = 0
            endValue = length
        elif endValue > maxLength:
            startValue = max(0, maxLength-length)
            endValue = maxLength

        spos = self.getClosestXPos(chart, startValue, GET_SMALLER)
        epos = self.getClosestXPos(chart, endValue, GET_LARGER)

        yMinValue, yMaxValue = self._getYMinMax(spos, epos+1)
        self.chartView.update(startValue, endValue, yMinValue, yMaxValue)

    def resetView(self):
        self.chartView = ChartView(self.xMinValue, self.xMaxValue,
                                   self.yMinValue, self.yMaxValue)

    def resetToOriginalData(self):
        if self.origChartValues:
            self.chartValues = self.origChartValues
            self.chartView.values = [] # force recompute of chart values

    def requiresRecomputeChartValues(self, minItems, maxItems):
        if len(self.chartView.values) < minItems:
            return True

        _, xFactor, _, _ = self.getChartUnits()

        if self.chartView.xMin*xFactor < self.chartView.xMinStoredValues:
            return True

        if self.chartView.xMax*xFactor > self.chartView.xMaxStoredValues:
            return True

        if self.chartView.highestGranularity or len(self.chartView.values) < maxItems:
            # we already zoomed in so far that computing additional values would result
            # in more finer granular values
            return False

        spos = self.getClosestXPos(self.chartView.values, self.chartView.xMin*xFactor, GET_SMALLER)
        epos = self.getClosestXPos(self.chartView.values, self.chartView.xMax*xFactor, GET_LARGER)

        return (epos - spos) < minItems

    def getAllChartValues(self):
        return self.chartValues

    def setChartValues(self, values):
        self.chartValues = values
        prevX = None
        totalLength = 0
        self.yMinValue = None
        self.yMaxValue = None
        self.xMinValue = 0

        for x,y in self.chartValues:
            totalLength += (x-prevX) if not prevX is None else 0

            if self.yMinValue is None:
                self.yMinValue = y
                self.yMaxValue = y
            else:
                self.yMinValue = min(self.yMinValue, y)
                self.yMaxValue = max(self.yMaxValue, y)

            prevX = x

        self.xMaxValue = totalLength

        self.chartView.values = [] # force recompute of chart values

    def getChartValues(self, maxItems):
        stime = time.time()
        chart = self.chartValues
        l = len(chart)

        offset = int(maxItems * 0.2) # left and right offset to store more values to handle zoom out and drag

        spos = self.getClosestXPos(chart, self.chartView.xMin, GET_SMALLER)
        epos = self.getClosestXPos(chart, self.chartView.xMax, GET_LARGER)
        rangeLength = epos+1 - spos

        spos = 0 if (spos-offset)<0 else spos-offset
        epos = l-1 if (epos+offset)>=l else epos+offset

        xUnit, xFactor, yUnit, yFactor = self.getChartUnits()

        step = rangeLength / maxItems
        if step < 1:
            step = 1

        result = []
        i = spos
        pos = spos
        while i <= epos:
            pos = int(i)
            x, y = chart[pos]
            x *= xFactor
            y *= yFactor
            result.append((x,y))
            i += step

        if pos < epos:
            x, y = chart[epos]
            x *= xFactor
            y *= yFactor
            result.append((x,y))

        etime = time.time()

        highestGranularity = step==1
        return result, highestGranularity

    def getCurrentChartRepresentation(self, minItems=None, maxItems=None):
        chart = self.chartValues
        l = len(chart)
        if not maxItems:
            maxItems = l
        elif l < maxItems:
            maxItems = l

        if not minItems:
            minItems = maxItems//2
        elif maxItems < minItems:
            minItems = maxItems

        recomputed = False
        if self.requiresRecomputeChartValues(minItems, maxItems):
            self.chartView.values, highestGranularity = self.getChartValues(maxItems)
            self.chartView.highestGranularity = highestGranularity
            recomputed = True

        result = self.chartView.values

        xUnit, xFactor, yUnit, yFactor = self.getChartUnits()

        return result, recomputed, self.chartView.xMin*xFactor, self.chartView.xMax*xFactor, xUnit, self.chartView.yMin*yFactor, self.chartView.yMax*yFactor, yUnit

    def smoothData(self, p, sampleFactor):
        if not self.origChartValues:
            self.origChartValues = self.chartValues

        sampleValues = bsplines.getSamplePoints(self.origChartValues, sampleFactor)
        knotVec = bsplines.getKnotVec(p, [x for x,_ in sampleValues])
        bs = bsplines.BSpline(knotVec, [y for _,y in sampleValues])

        smoothValues = []
        for x, y in self.origChartValues:
            v = bs.curve(p, x)
            smoothValues.append((x,v))

        self.setChartValues(smoothValues)

