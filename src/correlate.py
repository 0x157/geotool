import threading
from bisect import bisect
from datetime import datetime, timezone, timedelta
import multiprocessing
from .track import GeoPoint

def interpolLin(v1, v2, f):
    if v1 is None or v2 is None:
        return None

    return v1 + (v2-v1)*f

def correlate(time, points, timeGapInSeconds):
    i = bisect(points, time, key=lambda p: p.time)
    if i==0:
        return None # todo throw exception
    elif i==len(points):
        if points[i-1].time==time:
            p = points[i-1]
            return GeoPoint(p.latitude, p.longitude, p.altitude, None, p.speed)
        else:
            return None # todo throw exception

    p1 = points[i-1]
    p2 = points[i]

    if p1.time==time:
        return p1

    time_diff1 = abs((time-p1.time).total_seconds())
    time_diff2 = abs((time-p2.time).total_seconds())
    if time_diff1>timeGapInSeconds or time_diff2>timeGapInSeconds:
        return None

    lin_adj = (time-p1.time)/(p2.time-p1.time)
    point = GeoPoint(None, None, None, None, None)
    point.latitude = interpolLin(p1.latitude, p2.latitude, lin_adj)
    point.longitude = interpolLin(p1.longitude, p2.longitude, lin_adj)
    point.altitude = int(interpolLin(p1.altitude, p2.altitude, lin_adj))
    point.speed = interpolLin(p1.speed, p2.speed, lin_adj)
    point.time = None

    return point

KEEP_EXIF = 0
REPLACE_ALL = 1
REPLACE_NONE = 2

def correlateImage(image, timeOffset, points, timeGapInSeconds, replaceMode=REPLACE_NONE):
    preexist = not image.getCoords() is None
    if replaceMode==REPLACE_NONE and preexist:
        return False

    if replaceMode==KEEP_EXIF and image.coordsFromExif:
        return False

    dt = image.time
    if dt is None:
        return False

    dt -= timeOffset
    coords = correlate(dt, points, timeGapInSeconds)
    if not coords:
        return False

    if image.coords != coords:
        image.setCoords(coords)
        return True
    else:
        return False

class CorrelateImageThread(threading.Thread):
    def __init__(self, image, timeOffset, points, timeGapInSeconds, replaceMode=REPLACE_NONE):
        super().__init__()
        self.image = image
        self.timeOffset = timeOffset
        self.points = points
        self.timeGapInSeconds = timeGapInSeconds
        self.updated = False
        self.replaceMode = replaceMode

    def run(self):
        self.updated = correlateImage(self.image, self.timeOffset, self.points, self.timeGapInSeconds, self.replaceMode)

def waitForCorrelateThreads(threads):
    updatedImageIds = []
    for t in threads:
        t.join()
        if t.updated:
            updatedImageIds.append(t.image)
    return updatedImageIds

def tracksToPointList(tracks):
    points = []
    for track in tracks:
        points += track.getPoints()
    points.sort(key=lambda p: p.time)
    return points

def offsetAsTimeDelta(offset):
    offset = [] if len(offset) == 0 else offset.split(":")
    h = 0
    m = 0
    s = 0

    if len(offset) > 3:
        # todo throw error
        pass
    elif len(offset) == 3:
        h = int(offset[0])
        m = int(offset[1])
        s = int(offset[2])
    elif len(offset) == 2:
        h = int(offset[0])
        m = int(offset[1])
    elif len(offset) == 1:
        h = int(offset[0])

    return timedelta(hours=h, minutes=m, seconds=s)

def correlateImages(images, tracks, offset="", timeGapInSeconds=10, replaceMode=REPLACE_NONE):
    points = tracksToPointList(tracks)

    updatedImageIds = []

    timeOffset = offsetAsTimeDelta(offset)

    maxThreads = multiprocessing.cpu_count()*2
    activeThreads = []

    for image in images:
        if len(activeThreads) == maxThreads:
            updatedImageIds += waitForCorrelateThreads(activeThreads)
            activeThreads = []

        activeThreads.append(CorrelateImageThread(image, timeOffset, points, timeGapInSeconds, replaceMode))
        activeThreads[-1].start()

    updatedImageIds += waitForCorrelateThreads(activeThreads)
    return updatedImageIds
