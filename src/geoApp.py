from .track import Track
from .chart import Chart
from .images import Images

class GeoApp:
    def __init__(self):
        self.nextTrackId = 0
        self.nextImgId = 0
        self.tracks = {}
        self.charts = {}
        self.images = Images([])
        self.selectedTrackId = None

    def addTracks(self, files):
        trackIds = []

        for f in files:
            self.tracks[self.nextTrackId] = Track(f)
            trackIds.append(self.nextTrackId)
            self.nextTrackId += 1

        return trackIds

    def addImages(self, files):
        imgIds = self.images.addImages(files)
        return imgIds

    def setSelectedTrack(self, trackId):
        self.selectedTrackId = trackId

    def getSelectedTrack(self):
        return self.getTrack(self.selectedTrackId) if not self.selectedTrackId is None else None

    def getTracks(self):
        return self.tracks

    def getTrack(self, trackId):
        return self.tracks[trackId]

    def getChart(self, trackId, cType=None, forceNew=False, keepChartView=False):
        chart = self.charts[trackId] if trackId in self.charts else None
        if not chart or forceNew or ((not cType is None) and chart and chart.type != cType):
            chartView = None
            if keepChartView and chart:
                chartView = chart.chartView

            chart = Chart(self.getTrack(trackId), cType if (not cType is None) else Chart.DIST_ALTITUDE)

            if chartView:
                chart.chartView.xMin = chartView.xMin
                chart.chartView.xMax = chartView.xMax
                chart.chartView.yMin = chartView.yMin
                chart.chartView.yMax = chartView.yMax

            self.charts[trackId] = chart

        return chart

    def getImages(self):
        return self.images
