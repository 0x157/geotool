from PySide6.QtCore import QObject, QPointF, Signal

class GuiAccess(QObject):
    updateMap = Signal(str, arguments=['mapType'])

    setFocus = Signal(float, float, arguments=['lat', 'lon'])

    addMarker = Signal(str, float, float, str, int, arguments=['name', 'lat', 'lon', 'markercolor', 'level'])
    addImageMarker = Signal(str, float, float, str, int, str, arguments=['name', 'lat', 'lon', 'markercolor', 'level', 'imageSrc'])
    addTrackToMap = Signal(str, list, str, arguments=['trackId', 'coords', 'color'])
    addTrackToList = Signal(str, str, str, str, arguments=['trackId', 'name', 'desc', 'color'])
    updateTrackOfList = Signal(str, str, str, str, arguments=['trackId', 'name', 'desc', 'color'])

    setTracksLoadingIndicator = Signal(bool, arguments=['running'])

    removeMapItems = Signal(str, arguments=['name'])

    setTrackChart = Signal(str, list, float, float, str, float, float, str, str, arguments=['trackId', 'coords', 'xmin', 'xmax', 'xunit', 'ymin', 'ymax', 'yunit', 'color'])
    clearTrackChart = Signal()

    addImage = Signal(str, str, str, str, str, arguments=['imgId', 'name', 'path', 'coords', "datetaken"])
    setImage = Signal(str, str, str, str, str, arguments=['imgId', 'name', 'path', 'coords', "datetaken"])
    updateImageProgress = Signal(float, float, float, arguments=['from', 'to', 'value'])

    def __init__(self):
        super().__init__()

    def triggerUpdateMap(self, mapType):
        self.updateMap.emit(mapType)

    def triggerSetFocus(self, lat, lon):
        self.setFocus.emit(lat, lon)

    def triggerAddMarker(self, name, lat, lon, markercolor, level):
        self.addMarker.emit(name, lat, lon, markercolor, level)

    def triggerAddImageMarker(self, name, lat, lon, markercolor, level, path):
        self.addImageMarker.emit(name, lat, lon, markercolor, level, path)

    def triggerAddTrack(self, trackId, coords, name, desc, color):
        self.addTrackToMap.emit(trackId, coords, color)
        self.addTrackToList.emit(trackId, name, desc, color)

    def triggerUpdateTrackOnMap(self, trackId, coords, name, desc, color):
        trackId = str(trackId)
        self.removeMapItems.emit(trackId)
        self.addTrackToMap.emit(trackId, coords, color)
        self.updateTrackOfList.emit(trackId, name, desc, color)

    def triggerSetTracksLoadingIndicator(self, running):
        self.setTracksLoadingIndicator.emit(running)

    def triggerRemoveMapItems(self, name):
        self.removeMapItems.emit(name)

    def triggerSetTrackChart(self, trackId, chart, color):
        values, recomputed, xmin, xmax, xunit, ymin, ymax, yunit = chart.getCurrentChartRepresentation(300, 600)
        coords = []
        if recomputed:
            coords = [QPointF(p[0], p[1]) for p in values]
        self.setTrackChart.emit(str(trackId), coords, xmin, xmax, xunit, ymin, ymax, yunit, color)

    def triggerClearTrackChart(self):
        self.clearTrackChart.emit()

    def triggerAddImage(self, imgId, name, path, coords, datetaken):
        self.addImage.emit(imgId, name, path, coords, datetaken)

    def triggerSetImage(self, imgId, name, path, coords, datetaken):
        self.setImage.emit(imgId, name, path, coords, datetaken)

    def triggerUpdateImageProgress(self, fromVal, toVal, value):
        self.updateImageProgress.emit(fromVal, toVal, value)
