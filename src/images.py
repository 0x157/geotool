import os
import json
import subprocess
import multiprocessing
from datetime import datetime, timezone
from threading import Thread
from .track import GeoPoint

class Image:
    def _getExifData(self):
        r = subprocess.run(["exiftool", "-n", "-json", self.file], stdout=subprocess.PIPE)
        return json.loads(r.stdout)[0]

    def _getDateTaken(self, exif):
        if "DateTimeOriginal" not in exif:
            return None

        dt = datetime.strptime(exif['DateTimeOriginal'], '%Y:%m:%d %H:%M:%S')
        return datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, tzinfo=timezone.utc)

    def _getGeoInfo(self, exif):
        lat = exif.get("GPSLatitude", None)
        lon = exif.get("GPSLongitude", None)
        alt = exif.get("GPSAltitude", None)

        return GeoPoint(lat, lon, alt, None) if lat and lon else None

    def __init__(self, file, imgId):
        self.file = file
        self.imgId = imgId
        self.coords = None
        self.time = None
        self.coordsFromExif = False
        self.coordsChanged = False

    def fillFromExif(self):
        oldCoords = self.coords
        oldTime = self.time
        exif = self._getExifData()
        if not self.coords:
            self.coords = self._getGeoInfo(exif)
            self.coordsFromExif = not self.coords is None

        self.time = self._getDateTaken(exif)
        return oldCoords != self.coords or oldTime != self.time

    def getName(self):
        return os.path.basename(self.file)

    def getImagePath(self):
        return self.file

    def getId(self):
        return self.imgId

    def getCoords(self):
        return self.coords

    def hasCoords(self):
        return not self.coords is None

    def setCoords(self, coords):
        self.coordsChanged = self.coordsChanged or self.coords != coords
        self.coords = coords

    def setCoordsFromString(self, coords):
        l = coords.split("@")
        latLon = l[0]
        alt = None
        if len(l) > 1:
            alt = int(l[1])

        lat, lon = latLon.split(",")
        lat = float(lat)
        lon = float(lon)
        self.setCoords(GeoPoint(lat, lon, alt, None))

    def haveCoordsChanged(self):
        return self.coordsChanged

    def getDateTaken(self):
        return self.time

    def writeGeoInformation(self, keepOriginal=True):
        if not self.coords or not self.coordsChanged:
            return

        cmd = ["exiftool", "-preserve"]

        # for more information about the tags see https://exiftool.org/TagNames/GPS.html
        if self.coords.latitude:
            cmd.append("-GPSLatitude={}".format(abs(self.coords.latitude)))
            cmd.append("-GPSLatitudeRef={}".format("South" if self.coords.latitude<0 else "North"))
        if self.coords.longitude:
            cmd.append("-GPSLongitude={}".format(abs(self.coords.longitude)))
            cmd.append("-GPSLongitudeRef={}".format("West" if self.coords.longitude<0 else "East"))
        if self.coords.altitude:
            cmd.append("-GPSAltitude={}".format(abs(self.coords.altitude)))
            cmd.append("-GPSAltitudeRef={}".format("Below Sea Level" if self.coords.altitude<0 else "Above Sea Level"))
        if self.coords.speed:
            cmd.append("-GPSSpeed={}".format(self.coords.speed))
            #cmd.append("-GPSSpeedRef=K")

        cmd.append(self.file)
        if not keepOriginal:
            cmd.append("-overwrite_original_in_place")

        subprocess.run(cmd, stdout=subprocess.PIPE)
        self.coordsChanged = False
        self.coordsFromExif = True

class Images:
    def __init__(self, files):
        self.images = {}
        self.nextImgId = 0
        self.addImages(files)

    def fillFromExif(self):
        for img in self.images.values():
            img.fillFromExif()

    def addImages(self, files):
        imgIds = []

        for f in files:
            self.images[self.nextImgId] = Image(f, self.nextImgId)
            imgIds.append(self.nextImgId)
            self.nextImgId += 1

        return imgIds

    def getImages(self):
        return self.images

    def getImage(self, imgId):
        return self.images[imgId]

    def writeGeoInformation(self, keepOriginal=True, updateFct=None):
        maxThreads = multiprocessing.cpu_count()*2
        activeThreads = []

        def waitForThreads(threads):
            for t in threads:
                t.join()

        i = 0
        images = self.images.values()
        for image in images:
            if updateFct and i%10 == 0 and i < len(images)-1:
                updateFct(1, len(images), i+1)

            i += 1

            if not image.haveCoordsChanged():
                continue

            if len(activeThreads) == maxThreads:
                waitForThreads(activeThreads)
                activeThreads = []

            activeThreads.append(Thread(target=Image.writeGeoInformation, args=(image, keepOriginal)))
            activeThreads[-1].start()

        updateFct(1, len(images), i)

        waitForThreads(activeThreads)
