import os
import copy

import gpxpy
import gpxpy.gpx
from datetime import datetime, timezone
from threading import Thread
import time

from .utils import getDateRep, getXDeltaDistance, getDistanceInfoFromMeters
from . import bsplines


zeroTime = datetime(1, 1, 1, tzinfo=timezone.utc)

class GeoPoint:
    def __init__(self, lat, lon, alt, time, speed=None):
        self.latitude = lat
        self.longitude = lon
        self.altitude = alt
        self.time = time
        self.speed = speed

    def __repr__(self):
        return f"{self.latitude},{self.longitude}@{self.altitude}: {self.time}"

    def __eq__(self, other):
        if other is None:
            return False
        return self.latitude == other.latitude and self.longitude == other.longitude and self.altitude == other.altitude and self.time == other.time and self.speed == other.speed

class Track:
    DIST_ALTITUDE = "distalt"
    DIST_SPEED = "distspeed"
    DURATION_ALTITUDE = "durationalt"
    DURATION_SPEED = "durationspeed"

    DURATON_UNIT_SECONDS = "unitseconds"
    DISTANCE_UNIT_METERS = "meters"

    def saveAsGpx(self, path):
        gpx = gpxpy.gpx.GPX()
        gpx_track = gpxpy.gpx.GPXTrack()
        gpx.tracks.append(gpx_track)

        gpx_segment = gpxpy.gpx.GPXTrackSegment()
        gpx_track.segments.append(gpx_segment)

        for point in self.points:
            gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(point.latitude, point.longitude, elevation=point.altitude, time=point.time))

        with open(path, 'w', encoding="utf-8") as f:
            f.write(gpx.to_xml())

    def getGpsPoints(self, path):
        extension = os.path.splitext(path)[1].lower()
        if extension == ".gpx":
            return self.getGpsPointsFromGpx(path)
        if extension == ".fit":
            return self.getGpsPointsFromFit(path)
        else:
            raise Exception(f"unknown extension: {ext}")

    def getGpsPointsFromGpx(self, path):
        gps = None
        points = []

        with open(path, 'r') as f:
            gps = gpxpy.parse(f)

        if gps is None:
            return points

        for track in gps.tracks:
            for seg in track.segments:
                for point in seg.points:
                    if not point.time:
                        # gpx time should always be in UTC
                        point.time = zeroTime
                    points.append(GeoPoint(point.latitude, point.longitude, point.elevation, point.time))

        return points

    def getGpsPointsFromFit(self, path):
        # https://github.com/garmin/fit-python-sdk?tab=readme-ov-file
        from garmin_fit_sdk import Decoder, Stream

        stream = Stream.from_file(path)
        decoder = Decoder(stream)
        messages, errors = decoder.read()

        if len(errors) > 0:
            raise Exception("failed to read fit file: {errors}")
#print(errors)

        def semicircleToDegree(val):
            # fit-files store latitude/longitude in semicircles
            # https://forums.garmin.com/developer/fit-sdk/f/discussion/325061/what-crs-does-the-python-sdk-decode-to-eg-position_lat-485072248-position_long--882385675/1576901#1576901
            return (val*180)/(2**31)

        points = []
        records = messages["record_mesgs"]
        for r in records:
            lat = r.get("position_lat", None)
            lon = r.get("position_long", None)
            if not lat or not lon:
                continue

            lat = semicircleToDegree(lat)
            lon = semicircleToDegree(lon)

            elv = r.get("altitude", 0.0)
            # TODO check if timestamp is not in UTC?
            t = r.get("timestamp", zeroTime)
            #heartRate = r["heart_rate"]
            #temperature = r["temperature"]
            #speed= r["speed"]
            points.append(GeoPoint(lat, lon, elv, t))

        return points

    def __init__(self, path):
        self.name = os.path.splitext(os.path.basename(path))[0]
        self.minTime = None
        self.maxTime = None
        self.minAltitude = None
        self.maxAltitude = None
        self.totalUp = None
        self.totalDown = None
        self.points = []
        self.originalPoints = None
        self.totalDistance = 0

        self.initThread = Thread(target=Track._initFromGeoFile, args=(self, path))
        self.initThread.start()

    def _initFromGeoFile(self, path):
        self.points = self.getGpsPoints(path)
        self._initPoints()

    def _initPoints(self):
        prevPoint = None
        self.totalDistance = 0
        self.totalUp = 0
        self.totalDown = 0

        for point in self.points:
            if prevPoint:
                delta = getXDeltaDistance(point, prevPoint)
                self.totalDistance += delta

            if point.time:
                if not self.minTime or point.time < self.minTime:
                    self.minTime = point.time
                if not self.maxTime or point.time > self.maxTime:
                    self.maxTime = point.time

            if point.altitude:
                if not self.minAltitude or point.altitude < self.minAltitude:
                    self.minAltitude = point.altitude
                if not self.maxAltitude or point.altitude > self.maxAltitude:
                    self.maxAltitude = point.altitude

                if prevPoint and prevPoint.altitude:
                    if prevPoint.altitude < point.altitude:
                        self.totalUp += point.altitude - prevPoint.altitude
                    else:
                        self.totalDown += prevPoint.altitude - point.altitude

            prevPoint = point

    def resetToOriginalData(self):
        if self.originalPoints:
            self.points = copy.deepcopy(self.originalPoints)
            self._initPoints()

    def getPoints(self):
        if self.initThread.is_alive():
            self.initThread.join()
        return self.points

    def getDesc(self):
        unit, factor, strFormat = getDistanceInfoFromMeters(0, self.totalDistance, threshold=5000)
        distance = self.totalDistance * factor
        strFormat = f"{{:{strFormat}}}"
        distance = strFormat.format(distance)
        return f"""time (UTC): {getDateRep(self.minTime)} - {getDateRep(self.maxTime)}
distance: {distance} {unit}
altitude: {self.minAltitude:.0f} m – {self.maxAltitude:.0f} m
up: {self.totalUp:.0f} m
down: {self.totalDown:.0f} m"""

    def smoothImpl(self, degree, sampleFactor, getControlPoints, update):
        if not self.originalPoints:
            self.originalPoints = copy.deepcopy(self.points)

        def getTimeStamps(points):
            r = [p.time.timestamp() for p in points]
            f = r[0]
            return [t-f for t in r]

        samplePoints = bsplines.getSamplePoints(self.originalPoints, sampleFactor)

        knotVec = bsplines.getKnotVec(degree, getTimeStamps(samplePoints))
        bs = bsplines.BSpline(knotVec, getControlPoints(samplePoints))

        timeOffset = self.originalPoints[0].time.timestamp()
        smoothPoints = []
        for p in self.originalPoints:
            t = p.time.timestamp() - timeOffset
            v = bs.curve(degree, t)
            result = copy.copy(p)
            update(result, v)
            smoothPoints.append(result)

        self.points = smoothPoints
        self._initPoints()

    def smoothTrack(self, degree, sampleFactor):
        def getControlPoints(points):
            # todo include speed?
            return [(p.latitude, p.longitude, p.altitude) for p in points]

        def update(r, v):
            r.latitude = v[0]
            r.longitude = v[1]
            r.altitude = v[2]

        self.smoothImpl(degree, sampleFactor, getControlPoints, update)

    def smoothAltitude(self, degree, sampleFactor):
        def update(r, v):
            r.altitude = v
        self.smoothImpl(degree, sampleFactor, lambda points: [p.altitude for p in points], update)
