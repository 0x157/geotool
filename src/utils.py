from geopy.distance import geodesic
from datetime import timedelta

def getXDeltaDistance(currPoint, prevPoint):
    if prevPoint:
        # todo consider distance, there is convince function geopy.distance.lonlat
        return geodesic((prevPoint.latitude, prevPoint.longitude), (currPoint.latitude, currPoint.longitude)).meters
    else:
        return 0

def getXDeltaDuration(currPoint, prevPoint):
    if prevPoint:
        delta = currPoint.time - prevPoint.time
        return int(delta.total_seconds())
    else:
        return 0

def getYValueAltitude(currPoint, prevPoint):
    return currPoint.altitude if currPoint.altitude else 0

# return speed in km/h
def getYValueSpeed(currPoint, prevPoint):
    if prevPoint:
        deltaTime = (currPoint.time - prevPoint.time).total_seconds() / 3600
        deltaDist = (geodesic((prevPoint.latitude, prevPoint.longitude), (currPoint.latitude, currPoint.longitude)).meters) / 1000
        return deltaDist/deltaTime
    else:
        return 0.0

def getDistanceInfoFromMeters(minValue, maxValue, threshold=2000):
    if (maxValue - minValue) < threshold:
        return "m", 1.0, ".0f"
    else:
        return "km", 0.001, ".2f"

def getCoordRep(coords):
    if coords is None:
        return ""

    lat = f"{coords.latitude:.5f}".rstrip("0").rstrip(".")
    lon = f"{coords.longitude:.5f}".rstrip("0").rstrip(".")
    alt = coords.altitude
    if alt is None:
        return f"{lat},{lon}"
    else:
        return f"{lat},{lon}@{int(alt)}"

def getDateRep(date):
    return date.strftime("%Y-%m-%d %H:%M:%S") if date else ""
